<?php

use App\Http\Controllers\AlbumsController;
use App\Http\Controllers\ApiAuth\CaptchaController;
use App\Http\Controllers\ConfigurationController;
use App\Http\Controllers\GenresController;
use App\Http\Controllers\PlaylistItemsController;
use App\Http\Controllers\ApiAuth\UserAuthenticationController;
use App\Http\Controllers\ApiAuth\VerifyEmailController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\ApiAuth\RegisteredUserController;
use App\Http\Controllers\ApiAuth\NewPasswordController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\FeedController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function () {
    Route::get('public/albums', [AlbumsController::class, 'getAlbumsWithoutAuthentication']);
    Route::get('public/albums/{id}', [AlbumsController::class, 'getPublicAlbumById']);
    Route::get('public/genres', [GenresController::class, 'getGenreList']);
    Route::get('public/albums/{id}/playlist-items', [PlaylistItemsController::class, 'getPublicPlaylistItemsByAlbum']);

    Route::get('public/playlist-items', [PlaylistItemsController::class, 'getPlaylistItemsWithoutAuthentication']);
    Route::get('public/playlist-items/{id}', [PlaylistItemsController::class, 'getPublicPlaylistItemById']);

    Route::get('public/rss-feed', [FeedController::class, 'feed']);

    Route::get('configuration', [ConfigurationController::class, 'getConfiguration']);
    Route::post('captcha-request', [CaptchaController::class, 'captchaRequest']);
});

Route::prefix('v1')->middleware(['throttle:6,1'])->group(function () {
    Route::post('login', [UserAuthenticationController::class, 'login'])->name('login');
    Route::post('register', [RegisteredUserController::class, 'store'])->name('register');
});

Route::prefix('v1')->middleware(['auth:sanctum'])->group(function () {
    Route::post('logout', [UserAuthenticationController::class, 'logout']);

    // Profile endpoints
    Route::get('user/profile', [ProfileController::class, 'getUserProfile']);
    Route::patch('user/profile', [ProfileController::class, 'patchUserProfile']);
    Route::patch('user/change-password', [NewPasswordController::class, 'changePassword']);
    Route::get('user/connected-devices', [ProfileController::class, 'getAuthenticatedDevice']);
    Route::post('user/revoke-token', [ProfileController::class, 'revokeAuthenticationToken']);
});

Route::prefix('v1')->middleware(['auth:sanctum', 'throttle:6,1'])->group(function () {
    Route::get('verify-email/{id}/{hash}', VerifyEmailController::class)->name('verification.verify');
    Route::post('verification-notification', [EmailVerificationNotificationController::class, 'store'])->name('verification.send');
});

Route::prefix('v1')->middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::apiResource('albums', AlbumsController::class);
    Route::apiResource('playlist-items', PlaylistItemsController::class);
    Route::get('user-playlist', [PlaylistItemsController::class, 'getPlaylistItemsForAuthenticatedUser']);
    Route::get('bc-details', [AlbumsController::class, 'getBandcampAlbumDetails']);

    // Admin endpoints
    Route::get('user/role', [AdminController::class, 'isUserAdmin']);
    Route::get('admin/users', [AdminController::class, 'getUserList']);
    Route::patch('admin/users/{id}', [AdminController::class, 'updateUser']);
    Route::delete('admin/users/{id}', [AdminController::class, 'deleteUser']);
    Route::post('admin/configuration', [ConfigurationController::class, 'storeConfiguration']);
});
