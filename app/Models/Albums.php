<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Genres;
use App\Models\PlaylistItems;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Albums extends Model
{
    use HasFactory;

    protected $fillable = [
        'artist',
        'title',
        'url',
        'bandcampItemId',
        'bandcampItemType',
    ];

    public function playlistItems(): HasMany
    {
        return $this->hasMany(PlaylistItems::class, 'albums_id', 'id');
    }

    public function genres()
    {
        return $this->belongsToMany(Genres::class, 'albums_genres', 'albums_id', 'genres_id');
    }
}
