<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlaylistItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $note = $this->note;
        $comment = $this->comment;
        $favourite = $this->favourite;
        if ($this->user !== null) {
            $user = [
                'id' => $this->user->id,
                'name' => $this->user->name,
            ];

            if (!$this->listened) {
                if ($request->user() != null) {
                    if ($request->user()->id !== $this->user->id) {
                        $note = -1;
                        $comment = "";
                        $favourite = 0;
                    }
                } else {
                    $note = -1;
                    $comment = "";
                    $favourite = 0;
                }
            }
        } else {
            $user = [
                'id' => 0,
                'name' => '[user_no_longer_exists]'
            ];
        }



        return [
            'id' => $this->id,
            'note' => $note,
            'listened' => $this->listened,
            'listeningDate' => $this->listeningDate,
            'comment' => $comment,
            'isPrivate' => $this->isPrivate,
            'favourite' => $favourite,
            'user' => $user,
            'album' => new AlbumResource($this->album),
        ];
    }
}
