<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AlbumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $genres = [];
        foreach ($this->genres as $genre) {
            $genres[] = $genre->genre;
        }

        $average = -1;
        $sum = 0;
        $nb_listened = 0;
        $inConnectedUserPlaylist = false;
        foreach ($this->playlistItems as $playlistItem) {
            if ($request->user() != null) {
                if ($playlistItem->user_id === $request->user()->id) {
                    $inConnectedUserPlaylist = true;
                }
            }
            if ($playlistItem->listened && !$playlistItem->isPrivate) {
                $sum += $playlistItem->note*2;
                $nb_listened += 1;
            }
        }

        if ($nb_listened > 0) {
            $average = round($sum / $nb_listened, PHP_ROUND_HALF_DOWN);
            $average = $average / 2;
        }

        return [
            'id' => $this->id,
            'artist' => $this->artist,
            'title' => $this->title,
            'genre' => $genres,
            'url' => $this->url,
            'bandcampItemId' => $this->bandcampItemId,
            'bandcampItemType' => $this->bandcampItemType,
            'average' => $average,
            'inConnectedUserPlaylist' => $inConnectedUserPlaylist,
        ];
    }
}
