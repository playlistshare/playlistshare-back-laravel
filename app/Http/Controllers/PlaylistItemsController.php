<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlaylistItemsResource;
use App\Models\PlaylistItems;
use App\Models\Albums;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PlaylistItemsController extends Controller
{
    public function index(Request $request): AnonymousResourceCollection
    {
        return $this->getPlaylistItems($request, true);
    }

    public function getPlaylistItemsWithoutAuthentication(Request $request): AnonymousResourceCollection
    {
        return $this->getPlaylistItems($request, false);
    }

    public function getPublicPlaylistItemsByAlbum(Request $request, $id): AnonymousResourceCollection {
        $query = PlaylistItems::query()->where('albums_id', '=', $id);
        $query = $query->where('isPrivate', '=', false);
        $query = $query->orderByDesc('listeningDate');
        $items = $query->get();

        return PlaylistItemsResource::collection($items);
    }

    public function getPlaylistItemsForAuthenticatedUser(Request $request): AnonymousResourceCollection {
        return $this->getPlaylistItems($request, true, true);
    }

    public function getPlaylistItems(Request $request, $authenticated, $forAuthenticatedUser = false): AnonymousResourceCollection
    {
        // Pagination
        $limit = $request->get("limit", 5);

        // Filters
        $startDate = $request->get("start");
        $endDate = $request->get("end");
        $listened = $request->get("listened");
        $rating = $request->get("rating");
        $private = $request->get("private");
        $userId = $request->get("user_id");
        $favourite = $request->get("favourite");
        // Albums filters
        $genreFilter = $request->get("genre");
        // Generic filters
        $searchText = $request->get("search", "");
        $fullTextSearch = $request->get("fulltextsearch");

        // Sorting
        $sortingBy = ["listeningDate", "created_at"];
        $sortingParam = $request->get("sort", "");
        if ($sortingParam !== "") {
            $sortingBy = explode(",", $sortingParam);
        }

        // Filters parsing
        $query = PlaylistItems::query()->select('playlist_items.*');
        if ($startDate != null) {
            $dateFilter = \DateTime::createFromFormat("Ymd", $startDate);
            $query = $query->where("listeningDate", ">=", $dateFilter);
            $query = $query->whereNotNull("listeningDate");
        }
        if ($endDate != null) {
            $dateFilter = \DateTime::createFromFormat("Ymd", $endDate);
            $query = $query->where("listeningDate", "<=", $dateFilter->modify('+1 day'));
            $query = $query->whereNotNull("listeningDate");
        }
        if ($listened != null) {
            $query = $query->where("listened", "=", ($listened));
        }
        if ($rating != null) {
            $query = $query->where("note", "=", $rating);
        }
        if ($favourite != null) {
            $query = $query->where("favourite", "=", $favourite);
        }
        if (!$authenticated) {
            $query = $query->where("isPrivate", "=", false);
        } else {
            if ($request->user() == null) {
                $query = $query->where("isPrivate", "=", false);
            } else {
                if ($private != null) {
                    if ($private) {
                        $query = $query->where("isPrivate", "=", true);
                        $query = $query->where("user_id", "=", $request->user()->id);
                    } else {
                        $query = $query->where(function($query) use ($request) {
                            $query->where("isPrivate", "=", false)
                                    ->orWhere("user_id", "=", $request->user()->id);
                        });
                    }
                } else {
                    $query = $query->where(function($query) use ($request) {
                        $query->where("isPrivate", "=", false)
                                ->orWhere("user_id", "=", $request->user()->id);
                    });
                }
            }
        }

        if ($forAuthenticatedUser) {
            $query = $query->where("user_id", "=", $request->user()->id);
        } else {
            if ($userId != null) {
                $query = $query->where("user_id", "=", $userId);
            }
        }

        // Albums filter parsing
        if ($searchText != "") {
            $query = $query->join('albums', 'albums.id', '=', 'playlist_items.albums_id');

            if ($fullTextSearch != null && $fullTextSearch == 1) {
                // if fulltext search, then we search in the genre as well
                $genreFilterFulltext = Albums::query()->select('id');
                $genreFilterFulltext = $genreFilterFulltext->whereHas('genres', function ($q) use ($searchText) {
                    $q->where('genres.genre', "LIKE", '%' . $searchText . '%');
                });
                $query = $query->where(function($query) use ($searchText, $genreFilterFulltext) {
                    $query->where("albums.artist", "LIKE", '%' . $searchText . '%')
                    ->orWhere("albums.title", "LIKE", '%' . $searchText . '%')
                    ->orWhere("comment", "LIKE", '%' . $searchText . '%')
                    ->orWhereIn("albums_id", $genreFilterFulltext);
                });
            } else {
                $query = $query->where(function($query) use ($searchText) {
                    $query->where("albums.artist", "LIKE", '%' . $searchText . '%')
                    ->orWhere("albums.title", "LIKE", '%' . $searchText . '%')
                    ->orWhere("comment", "LIKE", '%' . $searchText . '%');
                });
            }
        }
        if ($genreFilter != null) {
            $genresArray = explode(",", $genreFilter);

            //I think we can optimize this, it is not that clean right now
            //On only one genre in the filter, it's ok though
            $albumFilter = Albums::query()->select('id');
            foreach ($genresArray as $genre) {
                $albumFilter = $albumFilter->whereHas('genres', function ($q) use ($genre) {
                    $q->whereIn('genres.genre', [$genre]);
                });
            }

            $query = $query->whereIn('albums_id', $albumFilter);
        }

        // Sorting handle
        foreach ($sortingBy as $sortingColumn) {
            $temp = explode("+", $sortingColumn);
            $sortType = "desc";
            $sortBy = $temp[0];

            if (count($temp) > 1) {
                $sortType = "asc";
            }

            if ($sortType == "asc") {
                $query = $query->orderBy($sortBy);
            } else {
                $query = $query->orderByDesc($sortBy);
            }
        }

        $items = $query->paginate($limit);
        return PlaylistItemsResource::collection($items);
    }

    public function show(PlaylistItems $playlistItem): PlaylistItemsResource
    {
        return new PlaylistItemsResource($playlistItem);
    }

    public function getPublicPlaylistItemById($id)
    {
        $playlistItem = PlaylistItems::query()->whereKey($id)->first();
        if ($playlistItem == null || $playlistItem->isPrivate) {
            return response()->json([
                'data' => 'Resource not found'
            ], 404);
        } else {
            return new PlaylistItemsResource($playlistItem);
        }
    }

    public function store(Request $request): JsonResponse
    {
        $playlistItem = new PlaylistItems([
            'note' => $request->input('note'),
            'listened' => $request->input('listened'),
            'listeningDate' => $request->input('listeningDate'),
            'comment' => $request->input('comment'),
            'isPrivate' => $request->input('isPrivate'),
            'favourite' => $request->input('favourite'),
        ]);

        $albumId = $request->input('albumId');
        if ($albumId !== null) {
            $album = Albums::find($albumId);
        } else {
            return response()->json([
                'message' => 'Bad request: album_id not found'
            ], 400);
        }

        // Check if the album is not currently into the user playlist
        $duplicateCheck = PlaylistItems::query()
                                ->where('albums_id', '=', $albumId)
                                ->where('user_id', '=', $request->user()->id)->count();

        if ($duplicateCheck > 0) {
            return response()->json([
                'message' => 'Item already exists in the user playlist'
            ], 200);
        }

        $playlistItem->album()->associate($album);
        $playlistItem->user()->associate($request->user());
        if ($playlistItem->album !== null) {
            $playlistItem->save();
        }

        return response()->json([
            'data' => new PlaylistItemsResource($playlistItem),
        ], 201);
    }

    public function update(Request $request, PlaylistItems $playlistItem): JsonResponse
    {
        $playlistItemUpdated = PlaylistItems::find($playlistItem->id);
        if ($playlistItemUpdated->user_id != $request->user()->id) {
            return response()->json([
                'message' => 'Playlist item not found'
            ], 404);
        }

        $playlistItemUpdated->update([
            'note' => $request->input('note'),
            'listened' => $request->input('listened'),
            'listeningDate' => $request->input('listeningDate'),
            'comment' => $request->input('comment'),
            'isPrivate' => $request->input('isPrivate'),
            'favourite' => $request->input('favourite'),
        ]);

        return response()->json([
            'data' => new PlaylistItemsResource($playlistItemUpdated)
        ], 200);
    }

    public function destroy(Request $request, PlaylistItems $playlistItem): JsonResponse
    {
        if ($playlistItem->user_id !== $request->user()->id) {
            return response()->json([
                'message' => 'Playlist item not found'
            ], 404);
        }

        $playlistItem->delete();
        return response()->json(null, 204);
    }
}
