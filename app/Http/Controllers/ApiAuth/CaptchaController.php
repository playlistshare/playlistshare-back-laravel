<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use IconCaptcha\IconCaptcha;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CaptchaController extends Controller
{
    function captchaRequest(Request $request) {
        $options = \IconCaptcha\Options::prepare([]);
        $options['token'] = null;
        $options['session']['driver'] = 'sqlite';
        $options['storage']['driver'] = 'sqlite';
        $options['storage']['connection'] = DB::connection()->getRawPdo();

        $captcha = new IconCaptcha($options);
        $captcha->request()->process();

        return response()->json('', 400);
    }
}
