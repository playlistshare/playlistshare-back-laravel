<?php

namespace App\Http\Controllers\ApiAuth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rules;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class NewPasswordController extends Controller {
    public function changePassword(Request $request) {
        $request->validate([
            'email' => ['required', 'email'],
            'token' => ['required'],
            'current_password' => ['required'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::find($request->user()->id);
        $hasher = app('hash');
        if (!$hasher->check($request->get('current_password'), $user->password)) {
            return response()->json([
                'status'=> 'error',
                'message' => 'Invalid password',
            ], 403);
        }

        // Change the password in database
        $user->forceFill([
            'password' => Hash::make($request->password),
            'remember_token' => Str::random(60),
        ])->save();

        event(new PasswordReset($user));

        return response()->json(['status' => 'ok']);
    }
}
