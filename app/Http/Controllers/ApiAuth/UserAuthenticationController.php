<?php

namespace App\Http\Controllers\ApiAuth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserAuthenticationController extends Controller
{
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'email' => ['required'],
            'password' => ['required'],
            'device_name' => ['required'],
        ]);

        $email = strtolower($request->input('email'));
        $password = $request->input('password');
        $device_name = $request->input('device_name');

        $credentials = [
            'email' => $email,
            'password' => $password,
        ];

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Invalid login credentials',
            ]);
        }

        $user = User::where('email', $email)->firstOrFail();
        if (!$user->hasVerifiedEmail()) {
            return response()->json([
                'message' => 'Account is not verified yet',
            ]);
        }

        Auth::setUser($user);

        $user->tokens()->where('name', $device_name)->delete();
        $token = $user->createToken($device_name)->plainTextToken;

        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
        ], 200);
    }

    public function logout(): JsonResponse
    {
        auth()->user()->tokens()->delete();
        Auth::forgetUser();

        return response()->json([
            'message' => 'Succesfully Logged out'
        ], 200);
    }
}
