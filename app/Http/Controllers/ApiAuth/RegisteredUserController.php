<?php

namespace App\Http\Controllers\ApiAuth;

use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\User;
use IconCaptcha\IconCaptcha;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\DB;

class RegisteredUserController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $config = Configuration::where('key_name', 'noregistration')->first();
        if ($config && $config->value === "1") {
            return response()->json([
                'message' => 'Resource not found',
                'test' => $request->input('name'),
            ], 404);
        }

        $options = \IconCaptcha\Options::prepare([]);
        $options['token'] = null;
        $options['session']['driver'] = 'sqlite';
        $options['storage']['driver'] = 'sqlite';
        $options['storage']['connection'] = DB::connection()->getRawPdo();

        $captcha = new IconCaptcha($options);

        $validation = $captcha->validate($_POST);
        if (!$validation->success()) {
            return response()->json([
                'status' => 'error',
                'message' => $validation->getErrorCode()
            ], 401);
        }

        $name = $request->input('username');
        $email = strtolower($request->input('email'));
        $password = $request->input('password');

        $request->validate([
            'username' => ['required', 'string', 'max:255', 'unique:'.User::class],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        try {
            $user = User::create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make($password),
            ]);

            event(new Registered($user));
        } catch (\PDOException $e) {
            return response()->json([
                'status'=> 'error',
                'message' => 'Error while creating the user: '.$e->getCode(),
            ], 500);
        }

        $token = $user->createToken('auth_token')->plainTextToken;
        Auth::setUser($user);

        return response()->json([
            'status' => 'success',
            'message' => 'User Account Created Successfully',
            'access_token' => $token,
            'token_type' => 'Bearer',
        ], 201);
    }
}
