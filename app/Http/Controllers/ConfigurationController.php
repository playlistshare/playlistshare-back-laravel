<?php

namespace App\Http\Controllers;
use App\Http\Resources\ConfigurationResource;
use App\Models\Configuration;
use Illuminate\Http\Request;

class ConfigurationController extends Controller {
    public function getConfiguration(Request $request) {
        $config = Configuration::all();
        $map = [];

        foreach ($config as $key => $value) {
            $map[$value->key_name] = $value->value;
        }

        return response()->json(["data" => $map], 200);
    }

    public function storeConfiguration(Request $request) {
        if (!$request->user()->is_admin) {
            return response()->json([
                'message' => 'Resource not found'
            ], 404);
        }

        $key = $request->get('key');
        $value = $request->get('value');

        $config = Configuration::where('key_name', $key)->first();

        if ($value) {
            if ($config) {
                $config->update(['value'=>$value]);
            } else {
                Configuration::create(['key_name' => $key, 'value' => $value]);
            }
        } else if ($config) {
            $config->delete();
        }

        return response()->json(["message" => "success"], 201);
    }
}
