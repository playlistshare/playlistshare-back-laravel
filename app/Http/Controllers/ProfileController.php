<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ProfileController extends Controller {
    public function getUserProfile(Request $request): JsonResponse {
        return response()->json([
            'data' => [
                'id' => $request->user()->id,
                'name' => $request->user()->name,
                'email' => $request->user()->email,
                'is_admin' => $request->user()->is_admin,
            ]
        ], 200);
    }

    public function patchUserProfile(Request $request): JsonResponse {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:'.User::class],
        ]);

        $user = User::find($request->user()->id);

        if ($request->input('name') !== null) {
            $user->name = $request->input('name');
            $user->save();
        }

        return response()->json([
            'data' => [
                'id' => $request->user()->id,
                'name' => $user->name,
                'email' => $request->user()->email,
            ]
        ], 200);
    }

    public function getAuthenticatedDevice(Request $request): JsonResponse {
        $tokens = auth()->user()->tokens;
        $devices = [];
        foreach ($tokens as $token) {
            $devices[] = $token->name;
        }

        return response()->json([
            'data' => $devices,
        ]);
    }

    public function revokeAuthenticationToken(Request $request): JsonResponse {
        $device_name = $request->input('device');
        auth()->user()->tokens()->where('name', $device_name)->delete();

        return response()->json([
            'message' => 'Succesfully Logged out from the device '.$device_name,
        ], 200);
    }
}
