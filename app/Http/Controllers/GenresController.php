<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Genres;
use Illuminate\Http\JsonResponse;

class GenresController extends Controller
{
    public function getGenreList(): JsonResponse
    {
        $genres = Genres::has('albums')->orderBy('genre')->pluck('genre');

        return response()->json([
            'data' => $genres
        ]);
    }
}
