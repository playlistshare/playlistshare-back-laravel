<?php

namespace App\Http\Controllers;

use App\Http\Resources\AlbumResource;
use App\Models\Albums;
use App\Models\Genres;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AlbumsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return $this->getAlbums($request, true);
    }

    public function getAlbumsWithoutAuthentication(Request $request): AnonymousResourceCollection
    {
        return $this->getAlbums($request, false);
    }

    private function getAlbums(Request $request, $authenticated): AnonymousResourceCollection
    {
        // Pagination
        $limit = $request->get("limit", 5);

        // Filters
        $genreFilter = $request->get("genre");
        $searchText = $request->get("search", "");
        $searchArtist = $request->get("artist", "");
        $searchTitle = $request->get("title", "");

        $query = Albums::query();


        if ($searchText != "") {
            $query = $query->where(function($query) use ($searchText) {
                $query->where("artist", "LIKE", '%' . $searchText . '%')
                    ->orWhere("title", "LIKE", '%' . $searchText . '%');
            });
        }
        if ($searchArtist != "") {
            $query = $query->where("artist", "LIKE", '%' . $searchArtist . '%');
        }
        if ($searchTitle != "") {
            $query = $query->where("title", "LIKE", '%' . $searchTitle . '%');
        }

        if ($genreFilter != null) {
            $genresArray = explode(",", $genreFilter);

            //I think we can optimize this, it is not that clean right now
            //On only one genre in the filter, it's ok though
            foreach ($genresArray as $genre) {
                $query = $query->whereHas('genres', function ($q) use ($genre) {
                    $q->whereIn('genres.genre', [$genre]);
                });
            }
        }

        $albums = $query->paginate($limit);
        return AlbumResource::collection(
            $albums
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $bandcampItem = $this->getBandcampAlbumDetailsByUrl($request->input('url'));

        $album = Albums::create([
            'artist' => $request->input('artist'),
            'title' => $request->input('title'),
            'genre' => "",
            'url' => $request->input('url'),
            'bandcampItemId' => $bandcampItem["data"]["id"],
            'bandcampItemType' => $bandcampItem["data"]["type"],
        ]);

        $genres = [];
        foreach ($request->input('genre') as $genre) {
            if ($genre != null) {
                $record = Genres::firstOrCreate(['genre' => $genre]);
                $genres[] = $record->id;
            }
        }
        $album->genres()->detach();
        $album->genres()->attach($genres);

        return response()->json([
            'data' => new AlbumResource($album)
        ], 201);
    }

    public function getPublicAlbumById($id)
    {
        $album = Albums::query()->whereKey($id)->first();
        if ($album == null || $album->isPrivate) {
            return response()->json([
                'data' => 'Resource not found'
            ], 404);
        } else {
            return new AlbumResource($album);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Albums $album): AlbumResource
    {
        return new AlbumResource($album);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Albums $album): JsonResponse
    {
        $album->update([
            'artist' => $request->input('artist'),
            'title' => $request->input('title'),
            'genre' => "",
            'url' => $request->input('url'),
        ]);

        $genres = [];
        foreach ($request->input('genre') as $genre) {
            if ($genre != null) {
                $record = Genres::firstOrCreate(['genre' => $genre]);
                $genres[] = $record->id;
            }
        }
        $album->genres()->detach();
        $album->genres()->attach($genres);

        return response()->json([
            'data' => new AlbumResource($album)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Albums $album): JsonResponse
    {
        $album->delete();
        return response()->json(null, 204);
    }

    public function getBandcampAlbumDetails(Request $request): array|string
    {
        $url = $request->get('url', '');
        return $this->getBandcampAlbumDetailsByUrl($url);
    }

    private function getBandcampAlbumDetailsByUrl($url): array|string
    {
        $details = ["type" => "", "id" => "", "artist" => "", "title" => ""];

        if (!preg_match("/(https?:\/\/)?([\d|\w]+)\.bandcamp\.com\/?.*/", $url)) {
            return [
                'data' => $details
            ];
        }

        // check with regex if this is a bandcamp url
        try {
            $tags = get_meta_tags($url);
            $complete_title = explode(", by", $tags['title']);
            $details["artist"] = trim($complete_title[count($complete_title) - 1]);
            $title = "";
            for ($i = 0; $i < (count($complete_title) - 1); $i++) {
                $title .= $complete_title[$i] . " ";
            }
            $details["title"] = trim($title);

            $prop_arr = explode(",", $tags['bc-page-properties']);
            for ($t = 0; $t < count($prop_arr); $t++) {
                $cur = str_replace(['{', '}', '&quot;', '\"', ' ', '/', '\'', '(', ')'], '', $prop_arr[$t]);
                $key = explode(":", $cur)[0];
                $val = explode(":", $cur)[1];
                if (str_starts_with($key, "item_id")) {
                    $details["id"] = $val;
                }
                if (str_starts_with($key, "item_type")) {
                    if ($val == 'a') {
                        $details["type"] = "album";
                    } else {
                        $details["type"] = "track";
                    }
                }
            }
        } catch (Exception $e) {
        }

        return [
            'data' => $details
        ];
    }
}
