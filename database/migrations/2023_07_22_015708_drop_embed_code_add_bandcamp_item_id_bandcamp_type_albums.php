<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->dropColumn('embedCode');
        });
        Schema::table('albums', function (Blueprint $table) {
            $table->text('bandcampItemId')->nullable();
        });
        Schema::table('albums', function (Blueprint $table) {
            $table->text('bandcampItemType')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->dropColumn('bandcampItemId');
        });
        Schema::table('albums', function (Blueprint $table) {
            $table->dropColumn('bandcampItemType');
        });
        Schema::table('albums', function (Blueprint $table) {
            $table->longText('bandcampItemId')->nullable();
        });
    }
};
