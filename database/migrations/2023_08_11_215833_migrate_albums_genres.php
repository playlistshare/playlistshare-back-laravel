<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Albums;
use App\Models\Genres;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $albums = Albums::all();
        foreach ($albums as $album) {
            $genres = array_map(fn($value) => trim($value),explode(",", $album["genre"]));
            $album_genres = [];

            foreach ($genres as $genre) {
                $record = Genres::firstOrCreate(['genre' => $genre]);
                $album_genres[] = $record->id;
            }

            $album->genres()->attach($album_genres);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
