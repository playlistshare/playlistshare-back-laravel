<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Albums;
use App\Models\User;
use App\Models\PlaylistItems;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $user = User::first();
        $albums = DB::table('albums')->get();
        foreach ($albums as $album) {
            $item = new PlaylistItems([
                'note' => $album->note,
                'listened' => $album->listened,
                'listeningDate' => $album->listeningDate,
                'comment' => $album->comment,
                'isPrivate' => $album->isPrivate,
                'created_at' => $album->created_at,
                'updated_at'=> $album->updated_at,
            ]);

            $item->album()->associate(Albums::find($album->id));
            $item->user()->associate($user);

            $item->save();
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
