<?php

namespace Database\Seeders;

use App\Models\Albums;
use App\Models\Genres;
use App\Models\PlaylistItems;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Albums::factory(10)->create()->each(function(Albums $album) {
            PlaylistItems::factory()->count(1)->create([
                'albums_id' => $album->id,
                'user_id' => User::all()->first()->id,
            ]);
        });
        Genres::factory(5)->create();

        $genres = Genres::all();

        Albums::all()->each(function ($album) use ($genres) {
            $album->genres()->attach(
                $genres->random(rand(1, 5))->pluck('id')->toArray()
            );
        });
    }
}
