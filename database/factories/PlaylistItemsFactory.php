<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PlaylistItems>
 */

class PlaylistItemsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        //get album

        return [
            'note' => $this->faker->numberBetween(0, 5),
            'listened' => $this->faker->boolean(75),
            'listeningDate' => $this->faker->dateTime(),
            'comment' => $this->faker->sentence(20),
        ];
    }
}
