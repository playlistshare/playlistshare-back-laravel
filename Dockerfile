FROM php:8.2-cli
RUN apt-get update -y && apt-get install -y openssl zip unzip git libonig-dev libmcrypt-dev libsqlite3-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev
RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring
WORKDIR /app
COPY . /app
COPY .env.example .env

# Environment variables
ENV APP_ENV=production
ENV APP_DEBUG=false
ENV LOG_CHANNEL=errorlog
ENV LOG_LEVEL=warning
ENV APP_URL=http://localhost:8000
ENV FRONTEND_URL=http://localhost:3000
ENV DB_DATABASE=/home/database.sqlite

# Environment variables for admin user
ENV ADMIN_NAME=test
ENV ADMIN_EMAIL=test@test.fr
ENV ADMIN_PASSWORD="12345678"

RUN composer install
RUN php artisan migrate --force
RUN php artisan key:generate
# caching stuff for production
RUN php artisan cache:clear
RUN php artisan config:cache
RUN php artisan route:cache
RUN php artisan view:cache

EXPOSE 8000
CMD php artisan serve --host 0.0.0.0 --port=8000
