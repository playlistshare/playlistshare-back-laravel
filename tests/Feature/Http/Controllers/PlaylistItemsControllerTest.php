<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Resources\PlaylistItemsResource;
use App\Http\Resources\AlbumResource;
use App\Models\Genres;
use App\Models\PlaylistItems;
use App\Models\Albums;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class PlaylistItemsControllerTest extends TestCase {
    public function testIndexReturnDataInValidFormat()
    {
        $this->withoutMiddleware();
        $this->json('get', 'api/v1/playlist-items')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        "*" => [
                            'id',
                            'note',
                            'listened',
                            'listeningDate',
                            'comment',
                            'isPrivate',
                            'favourite',
                            'user' => [
                                'id',
                                'name',
                            ],
                            'album' => [
                                'id',
                                'artist',
                                'title',
                                'genre',
                                'url',
                                'bandcampItemId',
                                'bandcampItemType',
                            ],
                        ]
                    ]
                ]
            );
    }

    public function testItemsByAlbumReturnDataInValidFormat() {
        $albums = AlbumResource::collection(Albums::all());
        $albumId = $albums[0]->id;

        $json = $this->json('get', "api/v1/public/albums/$albumId/playlist-items");
        $json->assertStatus(Response::HTTP_OK)
        ->assertJsonStructure(
            [
                'data' => [
                    "*" => [
                        'id',
                        'note',
                        'listened',
                        'listeningDate',
                        'comment',
                        'isPrivate',
                        'favourite',
                        'user' => [
                            'id',
                            'name',
                        ],
                        'album' => [
                            'id',
                            'artist',
                            'title',
                            'genre',
                            'url',
                            'bandcampItemId',
                            'bandcampItemType',
                        ],
                    ]
                ]
            ]
        );
    }

    public function testGetOneReturnsDataInValidFormat() {
        $user = User::all()->first();
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $playlistItems = PlaylistItemsResource::collection(PlaylistItems::all());
        $playlistItemId = $playlistItems[0]->id;
        $this->json('get', "api/v1/playlist-items/$playlistItemId")
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'note',
                        'listened',
                        'listeningDate',
                        'comment',
                        'isPrivate',
                        'favourite',
                        'user' => [
                            'id',
                            'name',
                        ],
                        'album' => [
                            'id',
                            'artist',
                            'title',
                            'genre',
                            'url',
                            'bandcampItemId',
                            'bandcampItemType',
                        ],
                    ]
                ]
            );
    }

    public function testGetPlaylistItemsPaginated() {
        $jsonStructure = [
            'data' => [
                "*" => [
                    'id',
                    'note',
                    'listened',
                    'listeningDate',
                    'comment',
                    'isPrivate',
                    'favourite',
                    'user' => [
                        'id',
                        'name',
                    ],
                    'album' => [
                        'id',
                        'artist',
                        'title',
                        'genre',
                        'url',
                        'bandcampItemId',
                        'bandcampItemType',
                    ],
                ]
            ],
            'links' => [
                "first",
                "last",
                "prev",
                "next"
            ],
            'meta' => [
                "current_page",
                "last_page",
            ]
        ];

        $genres = Genres::all();

        $this->json('get', 'api/v1/public/playlist-items', ['page' => 1, 'limit' => 2])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/playlist-items', ['page' => 1, 'limit' => 2, 'listened' => 1])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/playlist-items', ['page' => 1, 'limit' => 2, 'genre' => $genres[0]->genre])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/playlist-items', ['page' => 1, 'limit' => 5, 'listened' => 0, 'genre' => $genres[0]->genre])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/playlist-items', ['page' => 1, 'limit' => 2, 'start' => "19900912", 'end' => "20230918"])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);
    }

    public function testCreate() {
        $album = [
            'artist' => $this->faker->word,
            'title' => $this->faker->word,
            'url' => $this->faker->url,
        ];
        $dbAlbum = Albums::create($album);
        $albumId = $dbAlbum->id;

        $user = User::all()->first();
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $payload = [
            "albumId" => $albumId,
            "note" => 3,
            "listened" => 1,
            "comment" => null,
            "isPrivate" => 0,
            "favourite" => 0,
        ];

        $json = $this->json('post', 'api/v1/playlist-items', $payload);
        $json_twice = $this->json('post', 'api/v1/playlist-items', $payload);

        $payload["albums_id"] = $payload["albumId"];
        unset($payload["albumId"]);

        $json->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'note',
                        'listened',
                        'listeningDate',
                        'comment',
                        'isPrivate',
                        'favourite',
                        'user' => [
                            'id',
                            'name',
                        ],
                        'album' => [
                            'id',
                            'artist',
                            'title',
                            'genre',
                            'url',
                            'bandcampItemId',
                            'bandcampItemType',
                        ],
                    ]
                ]
            );

        $json_twice->assertStatus(Response::HTTP_OK)
            ->assertJson(
                [
                    'message' => 'Item already exists in the user playlist'
                ]
            );

        $this->assertDatabaseHas('playlist_items', $payload);
    }

    public function testUpdate() {
        $playlistItem = PlaylistItems::all()->first();
        $id = $playlistItem->id;
        $user = User::find($playlistItem->user_id);
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $payload = [
            "id" => $id,
            "note" => 3,
            "listened" => true,
            "comment" => "Gros commentaire là",
            "isPrivate" => false,
            "favourite" => false,
        ];

        $json = $this->json('put', "api/v1/playlist-items/$id", $payload, ['Accept' => 'application/json']);

        $payload["id"] = $id;
        $payload["albums_id"] = $playlistItem->albums_id;

        $json->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'note',
                        'listened',
                        'listeningDate',
                        'comment',
                        'isPrivate',
                        'favourite',
                        'user' => [
                            'id',
                            'name',
                        ],
                        'album' => [
                            'id',
                            'artist',
                            'title',
                            'genre',
                            'url',
                            'bandcampItemId',
                            'bandcampItemType',
                        ],
                    ]
                ]
            );

        $this->assertDatabaseHas('playlist_items', $payload);
    }

    public function testUpdateForAnotherUser() {
        $user = User::create([
            'name' => 'test3',
            'email' => 'test5@test.fr',
            'password' => Hash::make('12345678'),
        ]);
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $playlistItem = PlaylistItems::all()->first();
        $id = $playlistItem->id;

        $payload = [
            "id" => $id,
            "note" => 3,
            "listened" => true,
            "comment" => "Gros commentaire là",
            "isPrivate" => false,
            "favourite" => true,
        ];

        $json = $this->json('put', "api/v1/playlist-items/$id", $payload, ['Accept' => 'application/json']);
        $json->assertStatus(Response::HTTP_NOT_FOUND);

        $payload["id"] = $id;
        $payload["albums_id"] = $playlistItem->albums_id;

        $this->assertDatabaseHas('playlist_items', ['id' => $payload['id']]);
        $this->assertDatabaseMissing('playlist_items', $payload);
    }

    public function testDelete() {
        $playlistItem = PlaylistItems::all()->first();
        $id = $playlistItem->id;
        $user = User::find($playlistItem->user_id);
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $this->json('delete', 'api/v1/playlist-items/' . $id)
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('playlist_items', ["id"=>$id]);
    }

    public function testDeleteForAnotherUser() {
        $user = User::create([
            'name' => 'test3',
            'email' => 'test4@test.fr',
            'password' => Hash::make('12345678'),
        ]);
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $playlistItem = PlaylistItems::all()->first();
        $id = $playlistItem->id;

        $this->json('delete', 'api/v1/playlist-items/' . $id)
            ->assertStatus(Response::HTTP_NOT_FOUND);

        $this->assertDatabaseHas('playlist_items', ["id"=>$id]);
    }
}
