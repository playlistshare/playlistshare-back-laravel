<?php

namespace Tests\Feature\Http\Controllers;

use App\Http\Resources\AlbumResource;
use App\Models\Albums;
use App\Models\Genres;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AlbumsControllerTest extends TestCase
{
    public function testIndexReturnsDataInValidFormat()
    {
        $this->withoutMiddleware();
        $this->json('get', 'api/v1/albums')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        "*" => [
                            'id',
                            'artist',
                            'title',
                            'genre',
                            'url',
                            'bandcampItemId',
                            'bandcampItemType',
                        ]
                    ]
                ]
            );
    }

    public function testGetOneReturnsDataInValidFormat()
    {
        $this->withoutMiddleware();
        $albums = AlbumResource::collection(Albums::all());
        $albumId = $albums[0]->id;
        $json = $this->json('get', 'api/v1/albums/' . $albumId);
        $json->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'artist',
                        'title',
                        'genre',
                        'url',
                        'bandcampItemId',
                        'bandcampItemType',
                    ]
                ]
            );
    }

    public function testPaginatedAlbum()
    {
        $jsonStructure = [
            'data' => [
                "*" => [
                    'id',
                    'artist',
                    'title',
                    'genre',
                    'url',
                    'bandcampItemId',
                    'bandcampItemType',
                ],
            ],
            'links' => [
                "first",
                "last",
                "prev",
                "next"
            ],
            'meta' => [
                "current_page",
                "last_page",
            ]
        ];

        $genres = Genres::all();

        $this->json('get', 'api/v1/public/albums', ['page' => 1, 'limit' => 2])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/albums', ['page' => 1, 'limit' => 2, 'listened' => 1])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/albums', ['page' => 1, 'limit' => 2, 'genre' => $genres[0]->genre])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/albums', ['page' => 1, 'limit' => 5, 'listened' => 0, 'genre' => $genres[0]->genre])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);

        $this->json('get', 'api/v1/public/albums', ['page' => 1, 'limit' => 2, 'start' => "19900912", 'end' => "20230918"])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure($jsonStructure);
    }

    public function testCreation()
    {
        $this->withoutMiddleware();
        $payload = [
            'artist' => $this->faker->word,
            'title' => $this->faker->word,
            'genre' => [$this->faker->word],
            'url' => $this->faker->url,
        ];

        $this->json('post', 'api/v1/albums', $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'artist',
                        'title',
                        'genre',
                        'url',
                        'bandcampItemId',
                        'bandcampItemType',
                    ]
                ]
            );

        $genres = $payload["genre"];
        unset($payload["genre"]);

        $this->assertDatabaseHas('albums', $payload);
        $this->assertDatabaseHas('genres', ['genre' => $genres[0]]);
    }

    public function testUpdate()
    {
        $user = User::create([
            'name' => 'test2',
            'email' => 'test2@test.fr',
            'password' => Hash::make('12345678'),
        ]);
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');
        $album = [
            'artist' => $this->faker->word,
            'title' => $this->faker->word,
            'url' => $this->faker->url,
        ];
        $dbAlbum = Albums::create($album);
        $album["id"] = $dbAlbum->id;
        $this->assertDatabaseHas('albums', $album);

        $payload = [
            'id' => $dbAlbum->id,
            'artist' => $this->faker->word,
            'title' => $this->faker->word,
            'genre' => [$this->faker->word],
            'url' => $this->faker->url,
        ];

        $this->json('put', 'api/v1/albums/' . $dbAlbum->id, $payload, ['Accept' => 'application/json'])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'artist',
                        'title',
                        'genre',
                        'url',
                        'bandcampItemId',
                        'bandcampItemType',
                    ]
                ]
            );

        $genres = $payload["genre"];
        unset($payload["genre"]);

        $this->assertDatabaseHas('albums', $payload);
        $this->assertDatabaseHas('genres', ['genre' => $genres[0]]);
    }

    public function testDelete()
    {
        $user = User::create([
            'name' => 'test3',
            'email' => 'test3@test.fr',
            'password' => Hash::make('12345678'),
        ]);
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');
        $album = [
            'artist' => $this->faker->word,
            'title' => $this->faker->word,
            'url' => $this->faker->url,
        ];
        $dbAlbum = Albums::create($album);
        $this->assertDatabaseHas('albums', $album);

        $this->json('delete', 'api/v1/albums/' . $dbAlbum->id)
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $this->assertDatabaseMissing('albums', $album);
    }

    public function testIndexReturnsWithoutAuthentication()
    {
        $this->withMiddleware('sanctum');
        $this->json('get', 'api/v1/albums')
            ->assertStatus(Response::HTTP_OK);
    }
}
