<?php

namespace Http\Controllers;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AdminControllerTest extends TestCase
{
    public function testIsAdmin()
    {
        $user = User::first();
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $this->json('get', 'api/v1/user/role')
            ->assertStatus(200)
            ->assertJsonStructure([
                'is_admin'
            ]);
    }

    public function testGetUserReturnsWithInvalidUser()
    {
        $this->withMiddleware();
        $name = fake()->lastName;
        $email = fake()->email;
        $password = '12345678';

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'is_admin' => false,
        ]);

        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $this->json('get', 'api/v1/admin/users')
            ->assertStatus(404);
    }

    public function testGetUserInValidFormat() {
        $this->withMiddleware();
        $name = fake()->lastName;
        $email = fake()->email;
        $password = '12345678';

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
            'is_admin' => true,
        ]);

        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $this->json('get', 'api/v1/admin/users')
        ->assertStatus(200)
        ->assertJsonStructure(
            ['data' => [
                "*" => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'created_at',
                    'updated_at',
                    'is_admin',
                ]
            ]]
        );
    }
}
