<?php

namespace Http\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GenresControllerTest extends TestCase
{
    public function testGetMusicGenresReturnsDataInValidFormat()
    {
        $this->json('get', 'api/v1/public/genres')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'data' => [
                        "*" => [
                        ]
                    ]
                ]
            );
    }
}
