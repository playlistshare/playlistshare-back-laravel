# Multi-user

Some note about changes for multi-user implementation.

No distinction between frontend and backend right now, this feature will be a WIP for a while.

# Roadmap / Todo-List for multi-user

## Database modification

-   [x] Rename album to playlist item
-   [x] Add album table
-   [ ] Multiple url for one album
-   [x] Add user_id into playlist item
-   [x] Add roles into user (admin, user, guest)

## Database migration

-   [x] Move comment from from album to playlist item
-   [x] Put user_id into comment (use the first user id)
-   [x] Give first user the admin role

## Features to be added / modified

-   [x] Registering new user (with email validation)
-   [x] Separate the albums endpoint and the playlistItems endpoint
-   [ ] Secure the update & deletion for album (to avoid update an album already listen by multiple user, or to delete album on playlist)
-   [x] Get the playlist for one user
-   [x] Get the to-listen / listened for one album
-   [x] Captcha for the registration
-   [x] Add an album into the "to-listen" list
-   [x] Comment and rate an album into the "to-listen" list
-   [ ] Users can decide to hide the "to-listen" / listened list as they want.
-   [ ] User profile
-   [x] Rework the search endpoint

## Others

-   [x] General terms of use
-   [x] For the ToS and the Privacy Policy, create two markdown files and load it into React Component

## API Endpoints

### New endpoints (or changing endpoints)

-   Public endpoints:

    -   Old
        -   [x] `GET /api/v1/public/albums`
        -   [x] `GET /api/v1/public/albums/{id}`
    -   New
        -   [x] `GET /api/v1/public/playlist-items`
        -   [x] `GET /api/v1/public/playlist-items/{id}`

-   Authenticated endpoints:
    -   Old
        -   [x] `GET /api/v1/albums`
        -   [x] `GET /api/v1/albums/{id}`
        -   [x] `POST /api/v1/albums`
        -   [x] `PUT /api/v1/albums/{id}`
        -   [x] `DELETE /api/v1/albums/{id}`
    -   New
        -   [x] `GET /api/v1/playlist-items`
        -   [x] `GET /api/v1/playlist-items/{id}`
        -   [x] `POST /api/v1/playlist-items`
        -   [x] `PUT /api/v1/playlist-items/{id}`
        -   [x] `DELETE /api/v1/playlist-items/{id}`

### Old endpoints (for reference)

-   Public endpoints:

    -   `POST /api/v1/login`: User login.
    -   `GET /api/v1/public/albums`: Retrieve a list of shared albums (except private ones).
    -   `GET /api/v1/public/albums/{id}`: Retrieve a specific album by its ID (don't work on private album).
    -   `GET api/v1/public/genres`: Retrieve the list of genre

-   Authenticated endpoints:
    -   `POST /api/v1/register`: User registration.
    -   `POST /api/v1/logout`: User logout.
    -   `GET /api/v1/albums`: Retrieve a list of shared albums (include private ones).
    -   `GET /api/v1/albums/{id}`: Retrieve a specific album by its ID (work on private album).
    -   `POST /api/v1/albums`: Submit a new album.
    -   `PUT /api/v1/albums/{id}`: Update an existing album.
    -   `DELETE /api/v1/albums/{id}`: Delete a shared album.
