# Playlist Share Backend

This repository contains the backend server that powers the ReactJS frontend for my Playlist Share project. 

The backend provides the necessary API endpoints and data management to handle user authentication, album submissions, and retrieval of shared albums.

This project is still in development, you can use it in production if you wish, but note that this comes with no guaranty.


## Features

* User Authentication: Provides user registration, login, and logout functionalities. (for now, registration is a protected route)
* Album Management: Handles album submissions, notations, and comments from users.
* Database Integration: Utilizes a database (e.g., SQLite, MySQL, PostgreSQL) to store user information, albums, and related data.
* API Endpoints: Exposes RESTful API endpoints to interact with the frontend and handle CRUD operations.
* Authorization: Implements access control to ensure only authenticated users can submit or modify albums.

## Prerequisites

Before running the backend, ensure you have the following prerequisites installed:

* PHP (>= 8.1)
* Composer (Dependency Manager for PHP)
* A database server (optional, by default, this use a SQLite database)

## Getting started

Follow these instructions to set up and run the backend server locally:

1. Clone the repository:

       git clone https://framagit.org/blchrd/playlist-back-laravel.git
       cd playlist-back-laravel

2. Install the PHP dependencies:

       composer install

3. Configure environment variables: rename the `.env.example` file to `.env` and update the necessary environment variables such as database credentials (see [Configuration](#Configuration))

4. Generate application key:

       php artisan key:generate

5. Set up the database (if you want to use SQLite, you can skip this step): create a new database for the application and update the `DB_*` variables in the `.env` file accordingly

6. Run database migrations:

       php artisan migrate

7. Start the development server:

       php artisan serve

The backend will be accessible at `http://localhost:8000`

## Configuration

Environment variable to be define (beside the `DB_*` for database):
- `APP_URL`: URL for application
- `FRONTEND_URL`: Frontend URL
- `ADMIN_NAME`: Name of admin user, you have to change this
- `ADMIN_EMAIL`: Email of admin user, you have to change this
- `ADMIN_PASSWORD`: Password of admin user, you have to change this

## API endpoints

**Warning**: This documention is not updated.

To get an updated documentation, please use the [Laravel Request Doc](https://github.com/rakutentech/laravel-request-docs/blob/master/README.md) of the project.

The backend provides the following API endpoints for the frontend application:

* Public endpoints:
  * `POST /api/v1/login`: User login.
  * `GET /api/v1/public/albums`: Retrieve a list of shared albums (except private ones).
  * `GET /api/v1/public/albums/{id}`: Retrieve a specific album by its ID (don't work on private album).
  * `GET api/v1/public/genres`: Retrieve the list of genre

* Authenticated endpoints:
  * `POST /api/v1/register`: User registration.
  * `POST /api/v1/logout`: User logout.
  * `GET /api/v1/albums`: Retrieve a list of shared albums (include private ones).
  * `GET /api/v1/albums/{id}`: Retrieve a specific album by its ID (work on private album).
  * `POST /api/v1/albums`: Submit a new album.
  * `PUT /api/v1/albums/{id}`: Update an existing album.
  * `DELETE /api/v1/albums/{id}`: Delete a shared album.

_Replace `{id}` with the actual ID of the album when applicable._

For the `albums` endpoint (both public and authenticated), you can add url parameters to filter and / or sort the results, here is the references:

| Parameter Name | Type            | Description                                  | Example   |
|----------------|-----------------|----------------------------------------------|-----------|
| `search` | string | Full text research (on artist, title and comment) | `search=great%20album` |
| `artist` | string | Text research on the artist name | `artist=World%27s%20End%20Girlfriend` |
| `title` | string | Text research on the album title | `title=Resistance%20%26%20The%20Blessing` |
| `listened` | number (0 or 1) | 1 to get the listened album, 0 to get the unlistened ones | `listened=0` |
| `rating` | number | Get the albums with the specified rating | `rating=5` |
| `start` | date with `Ymd` format | Get albums listened after the date | `start=20230901` |
| `end` | date with `Ymd` format | Get albums listened before the date | `end=20230930` |
| `private` | number (0 or 1) | 1 to get the private album, 0 to get the public ones, getting the private albums require authentication | `private=1` |
| `genre` | string | List of genre separated by `,`. It uses a `AND` operator, if you specified two genre, you'll get the album with this two genres, not albums with one genre or the other | `genre=Electro,Experimental` |
| `sort` | string | Sort the output with column name. **Only desc sort in implemented yet** | `sort=note` |

## Contributing

Thank you for your interest in contributing to Playlist Share! At the moment, we are not accepting direct contributions to this repository. However, you are welcome to fork the repository and work on any enhancements or modifications independently.

[See all repositories for PlaylistShare](https://framagit.org/playlistshare/)


## License

This work is licensed under Mozilla Public License Version 2.0, see the [LICENSE](LICENSE) file for details.
